# ----------------------------------------------------------------------------
# Created By  : Junkai Ding
# Created Date: 25/03/2022
# ---------------------------------------------------------------------------
# -*- coding: UTF-8 -*-

import sys
import math
import requests
import json
import hashlib

EARTH_REDIUS = 6378.137

# compatible python version
bigVersion = int(sys.version[0:1])
smallVersion = int(sys.version[2:3])


def compatible():
    if bigVersion == 2 or (bigVersion == 3 and smallVersion < 4):
        reload(sys)
        sys.setdefaultencoding('utf-8')


def rad(d):
    return d * math.pi / 180.0


# Calculate the distance between 2 points
def getDistance(point1, point2):
    lng1 = point1[0]
    lat1 = point1[1]
    lng2 = point2[0]
    lat2 = point2[1]
    radLat1 = rad(lat1)
    radLat2 = rad(lat2)
    a = radLat1 - radLat2
    b = rad(lng1) - rad(lng2)
    s = 2 * math.asin(
        math.sqrt(math.pow(math.sin(a / 2), 2) + math.cos(radLat1) * math.cos(radLat2) * math.pow(math.sin(b / 2), 2)))
    s = s * EARTH_REDIUS
    return s


class Earthquake:
    # init
    def __init__(self):
        self.point = [0, 0]
        self.inputLat()
        self.inputLong()
        self.getData()
        self.makeData()
        self.caldistance()
        self.output()

    # Obtain Seismological Bureau data
    def getData(self):
        res = requests.get('https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_month.geojson')
        if res.status_code == 200:
            data = json.loads(res.text)
            self.data = data['features']
        else:
            print("get data error")
            exit()

    # Processing data
    def makeData(self):
        self.list = {}
        for vo in self.data:
            dic = {}
            point = vo['geometry']['coordinates'][0:2]
            r = self.getKey(point)
            self.list[r] = {'point': point, 'title': vo['properties']['title']}

    # Input coordinate data of longitude
    def inputLong(self, is_retry=False):
        if is_retry == False:
            if bigVersion == 2:
                long = raw_input(u"Please input longitude(Enter Q to exit the program) , example:-78.1505\n")
            else:
                long = input("Please input the longitude (exit if input q or Q), example:-78.1505\n")
        else:
            if bigVersion == 2:
                long = raw_input(
                    u"Input error, Please re-enter longitude(Enter Q to exit the program) , example:-78.1505\n")
            else:
                long = input("Error, Please input the longitude again (exit if input q or Q), example:-78.1505\n")

        try:
            long = float(long)
            if long < -180 or long > 180:
                self.inputLong(True)
            self.point[0] = long
            print("Please wait for a moment while analyzing...")
        except Exception as e:
            if long.upper() == "Q":
                exit()
            else:
                self.inputLong(True)

    # Input coordinate data of latitude
    def inputLat(self, is_retry=False):
        if is_retry == False:
            if bigVersion == 2:
                lat = raw_input(u"Please enter latitude(Enter Q to exit the program) , example:42.5923\n")
            else:
                lat = input("Please input the latitude (exit if input q or Q) , example:42.5923\n")
        else:
            if bigVersion == 2:
                lat = raw_input(
                    u"Input error, Please re-enter latitude(Enter Q to exit the program) , example:42.5923\n")
            else:
                lat = input("Error, Please input the latitude again (exit if input q or Q) , example:42.5923\n")

        try:
            lat = float(lat)
            if lat < -90 or lat > 90:
                self.inputLat(True)
            self.point[1] = lat
        except Exception as e:
            if lat.upper() == "Q":
                exit()
            else:
                self.inputLat(True)

    # Calculate the distance from each seismic point
    def caldistance(self):
        self.disdic = {}
        for v in self.list:
            self.disdic[v] = int(round(getDistance(self.point, self.list[v]['point']), 0))

        self.disdic = sorted(self.disdic.items(), key=lambda kv: (kv[1], kv[0]))

    # Output results
    def output(self):
        print("Result")
        for i in range(10):
            print("{} || {}".format(self.list[self.disdic[i][0]]['title'], str(self.disdic[i][1])))

    # Use of internal processing data
    def getKey(self, point):
        return hashlib.md5(str(point).encode(encoding='UTF-8')).hexdigest()


if __name__ == '__main__':
    compatible()
    try:
        while True:
            Earthquake()
    except Exception as e:
        print("####....", e)
